
(function () {
  'use strict'
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  tooltipTriggerList.forEach(function (tooltipTriggerEl) {
    new bootstrap.Tooltip(tooltipTriggerEl)
  })

})()


// Tree View Toggle on LSO Page
var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[ i ].addEventListener("click", function () {
    this.parentElement
      .querySelector(".nested")
      .classList.toggle("active");
    this.classList.toggle("caret-down");
  });
}
// Tree View Toggle on LSO Page

// issue date start

function wow(value) {
  switch (value) {
      case "3":
          document.getElementById("startDate").setAttribute("class", "d-block");
          break;
      default:
          document.getElementById("startDate").setAttribute("class", "d-none");
          break;
  }
  }

// issue date end
// show more
var l=0;
var o=0;
var t=0;
function show(){
    if(!l){
        document.getElementById("more").style.display = "inline";
        document.getElementById("less").style.display = "none";
        document.getElementById("show").innerHTML="SHOW LESS";
        l=1;
    }else{
        document.getElementById("more").style.display = "none";
        document.getElementById("less").style.display = "inline";
        document.getElementById("show").innerHTML="SHOW MORE";
        l=0;
    }
}
function online_show(){
    if(!o){
        document.getElementById("online-more").style.display = "inline";
        document.getElementById("online-less").style.display = "none";
        document.getElementById("online-show").innerHTML="SHOW LESS";
        o=1;
    }else{
        document.getElementById("online-more").style.display = "none";
        document.getElementById("online-less").style.display = "inline";
        document.getElementById("online-show").innerHTML="SHOW MORE";
        o=0;
    }
}
function type_show(){
    if(!t){
        document.getElementById("type-more").style.display = "inline";
        document.getElementById("type-less").style.display = "none";
        document.getElementById("type-show").innerHTML="SHOW LESS";
        t=1;
    }else{
        document.getElementById("type-more").style.display = "none";
        document.getElementById("type-less").style.display = "inline";
        document.getElementById("type-show").innerHTML="SHOW MORE";
        t=0;
    }
}
// show more end
// clear all start
function selectAll(){
  var inputs = document.querySelectorAll('.check');
        for (var i = 0; i < inputs.length; i++) {
            if(inputs[i].checked == false){
            inputs[i].checked = true;
            }else{
                inputs[i].checked = false;
            }
        }

}
    function deselectAll(){
        var inputs = document.querySelectorAll('.check');
        var checknone = document.querySelectorAll('.checkNone')
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].checked = false;
        }
        for (var i = 0; i < checknone.length; i++) {
            checknone[i].checked = false;
        }

}
    function onlineSelectAll(){
  var inputs = document.querySelectorAll('.check2');
        for (var i = 0; i < inputs.length; i++) {
            if(inputs[i].checked == false){
            inputs[i].checked = true;
            }else{
                inputs[i].checked = false;
            }
        }

}
    function onlineDeselectAll(){
        var inputs = document.querySelectorAll('.check2');
        var checknone = document.querySelectorAll('.checkNoneOnline')
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].checked = false;
        }
        for (var i = 0; i < checknone.length; i++) {
            checknone[i].checked = false;
        }

}
    function typeSelectAll(){
  var inputs = document.querySelectorAll('.check3');
        for (var i = 0; i < inputs.length; i++) {
            if(inputs[i].checked == false){
            inputs[i].checked = true;
            }else{
                inputs[i].checked = false;
            }
        }

}
    function typeDeselectAll(){
        var inputs = document.querySelectorAll('.check3');
        var checknone = document.querySelectorAll('.checkNoneType')
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].checked = false;
        }
        for (var i = 0; i < checknone.length; i++) {
            checknone[i].checked = false;
        }

}
// clear all end

// LSO Page address bar collapse functionality
// let pinIcon = document.getElementsByClassName("bi-pin-angle-fill");
// if(pinIcon == "bi-pin-angle-fill"){

  $('.address-c').click(function () {
  var addressCollapse=$( ".addresspin" ).hasClass( "text-primary" )
  console.log(addressCollapse)

  if(!addressCollapse) {
    $('.cls-address').addClass('d-none')
    $('.opn--address').removeClass('d-none')
    $('.address-summary-width').addClass('summary-width')
  }
})
  $('.project-c').click(function () {
  var projectCollapse=$( ".projectpin" ).hasClass( "text-primary" )
  console.log(projectCollapse)

  if(!projectCollapse) {
    $('.cls-project').addClass('d-none')
    $('.opn--project').removeClass('d-none')
    $('.address-summary-width').addClass('summary-width1')
  }
})
$('.address-close').click(function () {
  $('.cls-address').addClass('d-none')
  $('.opn--address').removeClass('d-none')
  $('.address-summary-width').addClass('summary-width')
})
$('.address-show').click(function () {
  $('.cls-address').removeClass('d-none').addClass('d-block')
  $('.opn--address').addClass('d-none').addClass('d-block')
  $('.address-summary-width').removeClass('summary-width')
})
// LSO Page project bar collapse functionality
$('.project-close').click(function () {
  $('.cls-project').addClass('d-none')
  $('.opn--project').removeClass('d-none')
  $('.address-summary-width').addClass('summary-width1')
})
$('.project-show').click(function () {
  $('.cls-project').removeClass('d-none').addClass('d-block')
  $('.opn--project').addClass('d-none').addClass('d-block')
  $('.address-summary-width').removeClass('summary-width1')
})

// Filter Bar Toggle
var menu_btn = document.querySelector("#menu-btn");
var sidebar = document.querySelector("#sidebar");
var container = document.querySelector(".my-container");
menu_btn.addEventListener("click", () => {
  sidebar.classList.toggle("active-nav");
  container.classList.toggle("active-cont");
});
function myFunction (x) {
  x.classList.toggle("bi-chevron-right");
}
// Filter Bar Toggle


// Closing Inner Modal
$('.closeInnermodal').click(function () {
  $('#innermodal').modal('hide')
})
// Closing Inner Modal


// Checkboxes

$("#tableSelect").click(function () {
  $(".tablecheck").prop('checked', $(this).prop('checked'));
});
$("#tableSelect2").click(function () {
  $(".tablecheck2").prop('checked', $(this).prop('checked'));
});
// Checkboxes


//date picker

$(document).ready(function() {
  $('#datepicker').datepicker();
});

$( function() {
  $( "#datepicker" ).datepicker();
} );
$( function() {
  $( "#datepickers" ).datepicker();
} );


// Table Pagination
jQuery(function ($) {
  var items = $("#datatable tbody tr");

  var numItems = items.length;
  var perPage = 8;

  // Only show the first 2 (or first `per_page`) items initially.
  items.slice(perPage).hide();

  // Now setup the pagination using the `#pagination` div.
  $("#pagination").pagination({
    items: numItems,
    itemsOnPage: perPage,
    cssStyle: "light-theme",

    // This is the actual page changing functionality.
    onPageClick: function (pageNumber) {
      // We need to show and hide `tr`s appropriately.
      var showFrom = perPage * (pageNumber - 1);
      var showTo = showFrom + perPage;

      // We'll first hide everything...
      items.hide()
        // ... and then only show the appropriate rows.
        .slice(showFrom, showTo).show();
    }
  });
});

function changeIcon (i) {
  i.classList.toggle("text-primary");

}
